<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class V2CategoriesControllerTest extends WebTestCase
{
    /** @test */
    public function GetCategoryWithoutAuthTest()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category');
        $this->assertEquals(401, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Invalid credentials."}', $client->getResponse()->getContent());
    }
    /** @test */
    public function GetCategoryWithAuthTest()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category', [], [], ['HTTP_apikey' => 'CORS']);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"category":[]}', $client->getResponse()->getContent());
    }
    /** @test */
    public function category_readTest()
    {
        $client = static::createClient();
        $client->request('GET', '/v2/category/azerty', [], [], ['HTTP_apikey' => 'CORS']);
        $this->assertEquals(406, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"msg":"Non numeric value","id":"azerty"}', $client->getResponse()->getContent());
    }
}

?>
